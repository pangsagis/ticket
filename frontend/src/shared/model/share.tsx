export interface OptionKey{
    value: string;
    text: string;
    color?: string;
}