export interface TicketModel{
    id?: number;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    created_timestamp?: string;
    updated_timestamp?: string;
}

export interface TicketModelFilter{
    id?: number;
    title?: string;
    description?: string;
    contact?: string;
    information?: string;
    status?: string;
    order_by?: string;
}