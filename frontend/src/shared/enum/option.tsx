import { OptionKey } from "../model/share";

export const statusOptions:OptionKey[] = [
    {value:'pending',text:'pending',color:'yellow'},
    {value:'accepted',text:'accepted',color:'green'},
    {value:'resolved',text:'resolved',color:'blue'},
    {value:'rejected',text:'rejected',color:'red'},
]

export const orderByOptions:OptionKey[] = [
    {value:'status',text:'status'},
    {value:'updated_timestamp',text:'lasted update'},
]