import { FC, useState, useEffect } from "react";

import { TicketDialog } from "./dialog/ticket-dialog";
import { TicketModel, TicketModelFilter } from "../../shared/model/ticket";
import { CardTicket } from "./card/ticket-card";
import { CircleButton } from "../mocule/circle-button-add";

import serviceTicket from "../../shared/service/ticket";
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { orderByOptions, statusOptions } from "../../shared/enum/option";

type TicketProps = {
  searchBy?: string | undefined;
};

export const Ticket: FC<TicketProps> = ({ searchBy }) => {
  const [ticketDialogIsOpen, setticketDialogIsOpen] = useState(false);
  const [tickets, setTickets] = useState<TicketModel[]>([]);
  const [selectedTicket, setSelectedTicket] = useState<TicketModel | null>(
    null
  );
  const [keySearch, setKeySearch] = useState<TicketModelFilter>({});

  useEffect(() => {
    if (searchBy || keySearch) {
      refreshTicket({ title: searchBy, ...keySearch });
    } else {
      refreshTicket();
    }
  }, [searchBy, keySearch]);

  const refreshTicket = (payload?: TicketModelFilter) => {
    serviceTicket.searchTicket(payload).then((tics) => {
      setTickets(tics);
    });
  };

  const onClickCard = (ticket: TicketModel) => {
    setSelectedTicket(ticket);
    setticketDialogIsOpen(true);
  };

  const onTicketDialogClose = () => {
    setticketDialogIsOpen(false);
  };

  const onTicketDialogSubmit = () => {
    setticketDialogIsOpen(false);
    refreshTicket({ title: searchBy, ...keySearch });
  };

  const onClickBtnAddTicket = () => {
    setSelectedTicket(null);
    setticketDialogIsOpen(true);
  };

  const handleChangeStatusSearch = (event: SelectChangeEvent) => {
    const status = event.target.value;
    setKeySearch({ ...keySearch, status: status });
  };

  const handleChangeOrderBy = (event: SelectChangeEvent) => {
    const value = event.target.value;
    setKeySearch({ ...keySearch, order_by: value });
  };

  return (
    <>
      <Box className="head-content">
        <FormControl sx={{ marginLeft: "10px" }}>
          <InputLabel id="id-label-filter-status">filter status</InputLabel>
          <Select
            labelId="id-label-filter-status"
            label="filter status"
            sx={{ width: "150px" }}
            value={keySearch?.status ?? ""}
            onChange={handleChangeStatusSearch}
            displayEmpty
            // inputProps={{ 'aria-label': 'Without label' }}
          >
            {[{ value: undefined, text: "all" }, ...statusOptions].map(
              ({ value, text }, i) => (
                <MenuItem key={i} value={value}>
                  {text}
                </MenuItem>
              )
            )}
          </Select>
        </FormControl>
        <FormControl sx={{ marginLeft: "10px" }}>
          <InputLabel id="sort-by-id">sort by</InputLabel>
          <Select
            labelId="sort-by-id"
            label="sort by"
            sx={{ width: "150px" }}
            value={keySearch?.order_by ?? ""}
            onChange={handleChangeOrderBy}
            displayEmpty
          >
            {orderByOptions.map(({ value, text }, i) => (
              <MenuItem key={i} value={value}>
                {text}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <Box className="body">
        <Box className="content">
          <Box className="d-flex flex-wrap justify-content-start">
            {tickets.length > 0
              ? tickets.map((ticket) => (
                  <CardTicket
                    key={ticket.id}
                    ticket={ticket}
                    onClick={() => {
                      onClickCard(ticket);
                    }}
                  />
                ))
              : "Empty Ticket"}
          </Box>
        </Box>
      </Box>

      <TicketDialog
        isOpen={ticketDialogIsOpen}
        onClose={onTicketDialogClose}
        onSubmit={onTicketDialogSubmit}
        ticket={selectedTicket}
      />

      <CircleButton onClick={onClickBtnAddTicket} />
    </>
  );
};
