import { FC } from "react";
import moment from "moment";

import { Card, CardHeader, CardContent, Typography } from "@mui/material";
import { TicketModel } from "../../../shared/model/ticket";
import { statusOptions } from "../../../shared/enum/option";
import Config from "../../../config";

type CardTicketProps = {
  ticket: TicketModel;
  onClick?: () => void;
};

export const CardTicket: FC<CardTicketProps> = ({ ticket, onClick }) => {
  return (
    <div style={{ margin: "10px" }}>
      <Card
        sx={{
          width: 300,
          transition: "transform 0.1s ease",
          "&:hover": {
            opacity: [0.9, 0.8, 0.7],
            transform: "scale(1.03)",
          },
        }}
        style={{ cursor: "pointer" }}
        onClick={onClick}
      >
        <CardHeader
          style={{ backgroundColor: "#00ADFF", textAlign: "center" }}
          title={ticket.title}
        />
        <CardContent style={{ backgroundColor: "#80D6FF" }}>
          <Typography component="p">
            info: {ticket.information ?? " - "}
          </Typography>
          <Typography component="p">
            contact: {ticket.contact ?? " - "}
          </Typography>
          <Typography sx={{ mb: 1 }}>
            status:
            <span
              style={{
                color:
                  statusOptions.find(({ value }) => value === ticket.status)
                    ?.color ?? "black",
                marginLeft: "5px",
                fontWeight: "bold",
              }}
            >
              {ticket.status}
            </span>
          </Typography>
          <Typography variant="body2" sx={{ mb: "3px" }}>
            description: {ticket.description ?? " - "}
          </Typography>
          <Typography component="p" sx={{ fontSize: "12px" }}>
            create :{" "}
            {moment(ticket.created_timestamp).format(
              Config.FORMAT.DATETIME.THAI
            )}
          </Typography>
          <Typography component="p" sx={{ fontSize: "12px" }}>
            update :{" "}
            {moment(ticket.updated_timestamp).format(
              Config.FORMAT.DATETIME.THAI
            )}
          </Typography>
        </CardContent>
      </Card>
    </div>
  );
};
