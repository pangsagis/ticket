const con = require('../connect/connect')

const { table_name } = require('../config.json').database

exports.initTable = () => {
    const sql = `
        CREATE TABLE IF NOT EXISTS public.${table_name.ticket} (
            id serial4 NOT NULL,
            title varchar NOT NULL,
            description varchar NULL,
            contact varchar NULL,
            information varchar NULL,
            status varchar NOT NULL DEFAULT 'pending'::character varying,
            created_timestamp timestamp NOT NULL DEFAULT now(),
            updated_timestamp timestamp NOT NULL DEFAULT now(),
            CONSTRAINT ${table_name.ticket}_pk PRIMARY KEY (id),
            CONSTRAINT ${table_name.ticket}_un UNIQUE (title)
        );
    `
    con.execute(sql, (error, res) => { })
}