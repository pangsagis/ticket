exports.bodyResponse = (res,kwargs)=>{
    body = {
        status: kwargs.status || true,
        data: kwargs.data || [],
        message: kwargs.message || '',
    }
    if (kwargs.error){
        body['error'] = kwargs.error
    }

    res
    .status(kwargs.status_code || 200)
    .json(body)
}