const { Pool } = require('pg')
const config = require('../config.json')
const database = config.database

exports.execute = (sql, cb) => {
  const con = new Pool({
    user: database.user,
    host: database.host,
    database: database.database,
    password: database.password,
    port: database.port,
  })
  con.query(sql, cb)
  con.end()
}