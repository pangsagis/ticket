const con = require('../connect/connect')
const { bodyResponse } = require('../utils/utils')

const { table_name } = require('../config.json').database

exports.readTicket = (req, res) => {
    const key_use_like = ['title', 'description', 'contact', 'information']

    const conditions = Object.entries(req.body)
        .filter(([key, value]) => !(key === 'order_by'))
        .map(([key, value]) => !key_use_like.includes(key) ? `${key}='${value}'` : `${key} like '%${value}%'`)

    const order_by = req.body.order_by ?? 'id';
    const sql = `select * from ${table_name.ticket} ${conditions.length > 0 ? `WHERE ${conditions.join(' and ')}` : ''}  ORDER BY ${order_by} DESC;`
    // console.log('sql = ',sql);
    con.execute(sql, (error, response) => {
        bodyResponse(res, {
            data: error ? [] : response.rows,
            error: error,
            message: ''
        })
    })
}

exports.createTicket = (req, res) => {
    const required_fields = ['title']
    const no_insert_keys = ['id'];
    const fixvalue_collumns = ['created_timestamp', 'updated_timestamp']
    const column_name = [];
    const column_value = [];
    Object.entries(req.body)
        .forEach(([key, value]) => {
            if (!no_insert_keys.includes(key) && !fixvalue_collumns.includes(key)) {
                column_name.push(key);
                column_value.push(value);
            }
            if (required_fields.includes(key)) {
                required_fields.splice(required_fields.indexOf(key), 1)
            }
        })

    if (required_fields.length > 0) {
        bodyResponse(res, {
            error: { required: required_fields },
            message: 'Create ticket failed'
        })
        return;
    }

    column_name.push(...fixvalue_collumns)
    column_value.push('now()', 'now()')

    const sql = `INSERT INTO ${table_name.ticket} (${column_name.join(',')}) VALUES ('${column_value.join("','").replace("''", null)}');`
    // console.log('sql = ',sql);
    con.execute(sql, (error, response) => {
        bodyResponse(res, {
            data: error ? [] : response.rows,
            error: error,
            message: error ? 'Create ticket failed' : 'Create ticket successful'
        })
    })
}

exports.updateTicket = (req, res) => {
    const condition_keys = ['id'];
    const collumns_no_update = ['created_timestamp', 'updated_timestamp']

    const conditions = Object.entries(req.body)
        .filter(([key, value]) => condition_keys.includes(key))
        .map(([key, value]) => `${key}='${value}'`)

    const data_toset = Object.entries(req.body)
        .filter(([key, value]) => !condition_keys.includes(key) && !collumns_no_update.includes(key))
        .map(([key, value]) => !value ? `${key}=null` : `${key}='${value}'`)
    data_toset.push(`updated_timestamp='now()'`)

    const sql = `UPDATE ${table_name.ticket} SET ${data_toset.join(',')} WHERE ${conditions.join(' and ')};`
    // console.log('sql = ',sql);
    con.execute(sql, (error, response) => {
        bodyResponse(res, {
            data: error ? [] : response.rows,
            error: error,
            message: error ? 'Update ticket failed' : 'Update ticket successful'
        })
    })
}
