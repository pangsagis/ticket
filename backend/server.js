const express = require('express')
const app = express()
const cors = require('cors');

const { port } = require('./config.json').server
const { initTable } = require('./init/init_table.js')

app.use(express.json());

app.use(cors({
  origin: ['http://localhost:3000']
}));

const routs = require('./routes')(app)
app.listen(port, () => {
  initTable()
  console.log(`server listening on port ${port}`)
})