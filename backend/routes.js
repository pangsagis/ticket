const routes_app = require('./app/routes')

module.exports = (app) => {
    app.use('/v1',routes_app)
}